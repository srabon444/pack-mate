# Pack Mate

Pack Mate application is for travel packing list, so you don't forget about your things. It  is created using React.js with Vite, Javascript, CSS, Zustand for state management.


## [Live Application](https://pack-mate-ten.vercel.app/)

![App Screenshot 1](public/1.png)
![App Screenshot 2](public/2.png)
![App Screenshot 3](public/3.png)
![App Screenshot 3](public/4.png)

## Features

- Add, Delete travel item to packing list.
- Sort by packed and unpacked items.
- Mark item as packed or unpacked.
- Mark all items as packed or unpacked.
- Reset the list to initial list.
- Remove all items from the list.
- Store the list in local storage.

## Tech Stack
- React Js
- Javascript
- CSS
- Zustand (State Management)

**Hosting:**
- Vercel


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/srabon444/pack-mate.git
```

Install dependencies

```bash
  npm install
```


