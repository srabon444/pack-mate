export const initialItems = [
  {
    id: 1,
    name: "Passport",
    packed: true,
  },
  {
    id: 2,
    name: "Ski Boots",
    packed: false,
  },
  {
    id: 3,
    name: "Thermal Jacket",
    packed: false,
  },
  {
    id: 4,
    name: "Suncream",
    packed: true,
  },
  {
    id: 5,
    name: "Snowboard",
    packed: true,
  }
];
