import { useContext } from "react";
import { ItemContext } from "../context/ItemsContextProvider.jsx";

export const useItemsContext = () => {
  const context = useContext(ItemContext);

  if (!context) {
    throw new Error(
      "useItemsContext must be used within an ItemContextProvider.",
    );
  }
  return context;
};
