export default function Footer() {
  return (
    <footer>
      <small>
        &copy; {new Date().getFullYear()} All Rights Reserved, by Ashraful Islam
      </small>
      <p>
        Version <b>1.7</b>
      </p>
    </footer>
  );
}
