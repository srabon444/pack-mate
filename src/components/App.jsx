import Footer from "./Footer.jsx";
import BackgroundHeading from "./BackgroundHeading.jsx";
import Header from "./Header.jsx";
import ItemList from "./ItemList.jsx";
import Sidebar from "./Sidebar.jsx";

function App() {
  return (
    <>
      <BackgroundHeading />

      <main>
          <Header />
          <ItemList />
          <Sidebar />
      </main>

      <Footer></Footer>
    </>
  );
}

export default App;
